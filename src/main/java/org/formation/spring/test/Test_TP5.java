package org.formation.spring.test;

import java.util.List;

import org.formation.spring.config.ApplicationConfig;
import org.formation.spring.model.Adresse;
import org.formation.spring.model.Client;
import org.formation.spring.service.IPrestiBanqueService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Test_TP5 {

	public static void main(String[] args) {
		ApplicationContext  context = new AnnotationConfigApplicationContext(ApplicationConfig.class);		   


		IPrestiBanqueService service = context.getBean("service", IPrestiBanqueService.class);
		List myList = service.listClients();
		
		Client cli1 = new Client("Gras", "Zlatka", "myLogin", "myPwd", new Adresse(13, "sa Rue", "sa Ville"));
		service.addClient(cli1);
		Client cli2 = new Client("Gras", "Vincent", "myLogin", "myPwd", new Adresse(13, "sa Rue", "sa Ville"));
		service.addClient(cli2);
		
		System.out.println(myList);
		
		

	    ((ConfigurableApplicationContext)(context)).close();
	    
	    

	}

}
